//Add default class to body
document.body.classList.add(localStorage.getItem('pageColor') || 'red');

var $el = document.querySelectorAll('.color-switcher li'),
    $classesList = [];

    //Loop On Element
for(var i = 0;i < $el.length; i++){
    //Get Classes List
    $classesList.push($el[i].getAttribute('data-color'));

    $el[i].addEventListener('click',function(){
        //Remove All Old Classes List
        document.body.classList.remove(...$classesList);

        //document.body.classList.remove('red green blue');


        //Add current Class From Li Aata Attribute
        document.body.classList.add(this.getAttribute('data-color'));

        //Add to Locale Storage
        localStorage.setItem('pageColor',this.getAttribute('data-color'));
    },false);
}

//console.log(localStorage.getItem('pageColor'));

//Remove LocalStorage
//localStorage.removeItem('pageColor');