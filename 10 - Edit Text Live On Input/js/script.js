var titleInput = document.getElementById('title'),
    contentInput = document.getElementById('content'),
    titleLive = document.getElementById('title-live'),
    contentLive = document.getElementById('content-live');


function copyText (fieldName,divName){
    fieldName.onkeyup = function (){
        "use strict";
        divName.textContent = this.value;
    }
}
copyText(titleInput,titleLive);
copyText(contentInput,contentLive);