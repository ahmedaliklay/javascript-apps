
var seconds = 10; //number of seconds
var countDiv = document.getElementById('countdown');
var secondPass;
var container = document.getElementById('container');
var countdown = setInterval(function(){
    "use strict";
    secondPass();
},1000);

function secondPass(){
    "use strict";
    var minutes = Math.floor(seconds / 60);
    var remSeconds = seconds % 60;

    if(seconds < 10){
        remSeconds = "0" + remSeconds;
    }

    countDiv.innerHTML = minutes + ":" + remSeconds;

    if(seconds > 0){
        seconds = seconds - 1;//decrease one second every 1000s 
    }else{
        clearInterval(countdown);
        countDiv.innerHTML = "Done";
        container.style.animation = 'none';
    }
}