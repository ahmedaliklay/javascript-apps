var goTop = document.getElementById('go-top'); 

window.onscroll = function (){

    "use strict";

    window.pageYOffset >= 800 ? goTop.style.display = "block" : goTop.style.display = "none";

    console.log(window.pageYOffset);

}

// goTop.onclick = function (){
//     "use strict";
//     window.scrollTo(0,0);
// }


goTop.addEventListener('click', () => window.scrollTo({
    top: 0,
    behavior: 'smooth',
}));
