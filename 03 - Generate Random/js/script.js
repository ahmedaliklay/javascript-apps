function generateSerial(){
    "use strict";

    var chars = "1234567890QWERTYUIOPLKJHGFDSAZXCVBNMqwertyuiolkjhgfdsazxcvbnm";

    var serialLength = 20;

    var randomSerial = '';

    var i;

    for(i = 0; i < serialLength; i++){
        var randerNumber = Math.floor(Math.random() * chars.length);
        randomSerial += chars.substring(randerNumber, randerNumber + 1);
    }
    document.getElementById('serial').innerHTML = randomSerial;
}