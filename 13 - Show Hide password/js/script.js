var  passwordField  = document.getElementById('password-field');

var  showPassword  = document.getElementById('show-password');

showPassword.onclick = function(){
    "use strict";
    if(this.textContent === "Show"){
        passwordField.setAttribute('type','text');
        this.textContent = "Hide";
    }else{
        passwordField.setAttribute('type','password');
        this.textContent = "Show";
    }
}